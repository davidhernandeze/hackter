let webpack, webpackDevMiddleware, webpackHotMiddleware, webpackConfig;
if (process.env.NODE_ENV !== 'production') {
    webpack = require('webpack');
    webpackDevMiddleware = require('webpack-dev-middleware');
    webpackConfig = require('../../webpack.config');
    webpackHotMiddleware = require('webpack-hot-middleware');
}

import { Server } from 'colyseus';
import http from 'http';
import express from 'express';
import path from 'path';
import basicAuth from 'express-basic-auth';
import { monitor } from '@colyseus/monitor';

import { ArenaRoom } from './rooms/ArenaRoom';

export const port = Number(process.env.PORT || 8080);
export const endpoint = 'localhost';

export let STATIC_DIR: string;

const app = express();
const gameServer = new Server({
    server: http.createServer(app),
    verifyClient: (info, next) => {
        const remoteIp = info.req.headers['x-real-ip'] || info.req.connection.remoteAddress;
        console.log('Incoming connection from: ' + remoteIp);
        next(true);
    }
});

gameServer.register('arena', ArenaRoom);

if (process.env.NODE_ENV !== 'production') {
    const webpackCompiler = webpack(webpackConfig({}));
    app.use(webpackDevMiddleware(webpackCompiler, {}));
    app.use(webpackHotMiddleware(webpackCompiler));

    STATIC_DIR = path.resolve(__dirname, '..', '..');

} else {
    STATIC_DIR = path.resolve(__dirname, '../dist');
    console.log(STATIC_DIR);
}

app.use('/', express.static(STATIC_DIR));

const auth = basicAuth({ users: { 'admin': 'admin' }, challenge: true });
app.use('/colyseus', auth, monitor(gameServer));

gameServer.listen(port);
console.log(`Listening on http://${endpoint}:${port}`);
