import { Schema, type } from "@colyseus/schema";

export class Entity extends Schema {
    @type("number") x: number;
    @type("number") y: number;
    @type("number") radius: number;
    @type("string") playerName: string;
    @type("string") message: string;

    dead: boolean = false;
    speed: number = 1;
    xSpeed = 0;
    ySpeed = 0;

    constructor(x: number, y: number, radius: number, playerName: string) {
        super();

        this.x = x;
        this.y = y;
        this.radius = radius;
        this.playerName = playerName;
        this.message = '';
    }

    static distance(a: Entity, b: Entity) {
        return Math.sqrt(Math.pow(b.x - a.x, 2) + Math.pow(b.y - a.y, 2));
    }
}
