import { Room, Client } from "colyseus";
import { Entity } from "./Entity";
import { State } from "./State";

export class ArenaRoom extends Room<State> {

  onInit() {
    this.setState(new State());
    this.state.initialize();
    this.setSimulationInterval(() => this.state.update());
  }

  onJoin(client: Client, options: any) {
    options.playerName = String(options.playerName).substr(0, 10);
    let date = Date().toString();
    console.log(`${options.playerName} joined the game at ${date}`);
    this.state.createPlayer(client.sessionId, options.playerName);
  }

  onMessage(client: Client, message: any) {
    const entity = this.state.entities[client.sessionId];

    // skip dead players
    if (!entity) {
      return;
    }

    const [command, data] = message;

    if (command === "move") {
      entity.xSpeed = data.x;
      entity.ySpeed = data.y;
    }
    if (command === "message") {
      entity.message === data.message ? data.message = data.message.concat(' ') : data.message;
      entity.message = data.message;
      console.log(`${entity.playerName} sent "${entity.message}"`)
    }
  }

  onLeave(client: Client) {
    const entity = this.state.entities[client.sessionId];
    let date = Date().toString();
    console.log(`${entity.playerName} left the game at ${date}`);
    if (entity) { entity.dead = true; }
  }

}
