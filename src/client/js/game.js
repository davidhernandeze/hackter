import * as PIXI from 'pixi.js'
import { Viewport } from 'pixi-viewport'
import { Client } from 'colyseus.js'
import Player from './player'
import { PLAYER_SPEED } from './player.js'
import Console from './console'
import HelpModal from './help-modal'
const WORLD_SIZE = 2000
const ENDPOINT = (process.env.NODE_ENV === 'development')
  ? 'ws://localhost:8080'
  : 'wss://hackter.io'

export const WIDTH = window.innerWidth
export const HEIGHT = innerHeight - 60
export const lerp = (a, b, t) => (b - a) * t + a

export default class {
  constructor (containerElement, inputElement, playerName) {
    this.playerName = playerName
    this.app = new PIXI.Application({
      width: WIDTH,
      height: HEIGHT,
      antialias: true,
      transparent: false,
      resolution: 1,
      backgroundColor: 0xfffff
    })
    this.containerElement = containerElement
    containerElement.insertBefore(this.app.view, containerElement.firstChild)

    this.viewport = new Viewport({
      screenWidth: window.innerWidth,
      screenHeight: window.innerHeight,
      worldWidth: WORLD_SIZE,
      worldHeight: WORLD_SIZE,
      interaction: this.app.renderer.plugins.interaction
    })
    this.app.stage.addChild(this.viewport)

    const boundaries = new PIXI.Graphics()
    boundaries.beginFill(0x002834)
    boundaries.drawRoundedRect(0, 0, WORLD_SIZE, WORLD_SIZE, 0)
    this.viewport.addChild(boundaries)

    const line = new PIXI.Graphics()
    for (let i = 0; i < WORLD_SIZE; i = i + 30) {
      line.lineStyle(2, 0x001D27)
        .moveTo(i, 0)
        .lineTo(i, WORLD_SIZE)
        .moveTo(0, i)
        .lineTo(WORLD_SIZE, i)
    }
    this.viewport.addChild(line)

    this.entities = {}
    this.client = new Client(ENDPOINT)
    this.room = this.client.join('arena', { playerName: this.playerName })
    this.room.onJoin.add(() => {
      this.initializeSchema()
      this.loop()
    })

    this.console = new Console(this.room)
    this.console.setupInput(inputElement)
    this.app.stage.addChild(this.console)

    this.helpModal = new HelpModal()
    this.app.stage.addChild(this.helpModal)
  }

  initializeSchema () {
    this.room.state.entities.onAdd = (entity, sessionId) => {
      let player = new Player(entity)
      this.viewport.addChild(player)
      this.entities[sessionId] = player
      player.x = entity.x
      player.y = entity.y
      if (sessionId === this.room.sessionId) {
        this.currentPlayerEntity = player
        this.viewport.follow(this.currentPlayerEntity, { speed: PLAYER_SPEED - 0.1 })
      }

      entity.onChange = (changes) => {
        let messageChange = changes.filter(change => change.field === 'message')
        if (messageChange.length > 0) {
          player.setMessage(messageChange[0].value)
        }
      }
    }

    this.room.state.entities.onRemove = (_, sessionId) => {
      this.app.stage.removeChild(this.entities[sessionId])
      this.entities[sessionId].destroy()
      delete this.entities[sessionId]
    }
  }
  loop () {
    for (let id in this.entities) {
      this.entities[id].x = lerp(this.entities[id].x, this.room.state.entities[id].x, 0.2)
      this.entities[id].y = lerp(this.entities[id].y, this.room.state.entities[id].y, 0.2)
    }
    /* eslint no-undef:0 */
    requestAnimationFrame(this.loop.bind(this))
  }
}
