import { Graphics, Text, Container } from 'pixi.js'
export const PLAYER_SPEED = 2
export default class extends Container {
  constructor ({ playerName }) {
    super()
    let circle = new Graphics()
    circle.beginFill(0x42f465, 1)
    circle.drawCircle(0, 0, 20)
    this.addChild(circle)

    let playerTag = new Text(playerName, {
      fontFamily: '"Lucida Console", Monaco, monospace',
      fill: '#FFFFFF',
      fontSize: 15,
      align: 'center',
      wordWrap: true,
      wordWrapWidth: 200
    })
    playerTag.x = -4 * playerName.length
    playerTag.y = -35
    this.addChild(playerTag)

    this.message = new Text('', {
      fontFamily: '"Lucida Console", Monaco, monospace',
      fill: '#fffc7b',
      fontSize: 9,
      fontWeight: 'lighter',
      align: 'left',
      wordWrap: true,
      wordWrapWidth: 50
    })
    this.message.x = 22
    this.message.y = -5
    this.addChild(this.message)
  }

  setMessage (message) {
    this.message.text = message
    let previousMessage = this.message.text
    setTimeout(() => {
      if (this.message.text === previousMessage) {
        this.message.text = ''
      }
    }, 4000)
  }
}
