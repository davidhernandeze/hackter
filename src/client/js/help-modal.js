import { Graphics, Text, Container } from 'pixi.js'
import { WIDTH, HEIGHT } from './game.js'

export default class extends Container {
  constructor (room) {
    super()
    this.room = room
    let console = new Graphics()
    console.beginFill(0xffffff, 1)
    console.drawRect(WIDTH * 0.8, 0, WIDTH * 0.3, HEIGHT * 0.3)
    this.addChild(console)
    this.commandListRender = new Text('', {
      fontFamily: '"Lucida Console", Monaco, monospace',
      fill: '#000000',
      fontSize: 15,
      letterSpacing: 2,
      lineHeight: 25
    })
    this.commandListRender.x = WIDTH * 0.8 + 5
    this.commandListRender.y = 5
    this.addChild(this.commandListRender)
    this.commandListRender.text = 'Available commands:\n- up\n- down\n- left\n- right\n- stop\n- msg Your message'
  }
  drawCommands () {
    const commandsToPrint = this.commandHistory.slice(Math.max(this.commandHistory.length - 12, 0))
    this.commandListRender.text = commandsToPrint.reduce((string, command) => {
      if (command.length > 25) {
        command = command.substr(0, 25).concat('...')
      }
      return string + '> ' + command + '\n'
    }, '')
  }
}
