import { Graphics, Text, Container } from 'pixi.js'
import { WIDTH, HEIGHT } from './game.js'
import { PLAYER_SPEED } from './player.js'

export default class extends Container {
  constructor (room) {
    super()
    this.room = room
    let console = new Graphics()
    console.beginFill(0x001d27, 1)
    console.drawRect(0, (HEIGHT / 2) - 10, WIDTH * 0.3, HEIGHT)
    this.addChild(console)

    this.commandHistory = []
    for (let i = 0; i < 24; i++) {
      this.commandHistory.push('')
    }
    this.commandListRender = new Text('', {
      fontFamily: '"Lucida Console", Monaco, monospace',
      fill: '#FFFFFF',
      fontSize: 15,
      letterSpacing: 2,
      lineHeight: 25
    })
    this.commandListRender.x = 2
    this.commandListRender.y = HEIGHT / 2
    this.addChild(this.commandListRender)
  }
  setupInput (inputElement) {
    inputElement.focus()
    document.addEventListener('click', () => {
      inputElement.focus()
    })
    this.historyIndex = 0
    this.commandCount = 0
    inputElement.addEventListener('keyup', (event) => {
      if (event.key === 'Enter' && inputElement.value !== '') {
        let command = inputElement.value.toLowerCase().trim()
        this.commandHistory.push(command)
        this.sendCommand(command)
        inputElement.value = ''
        this.drawCommands()
        this.historyIndex = 0
        this.commandCount++
      }
      if (event.key === 'ArrowUp') {
        if (this.commandCount > this.historyIndex) {
          this.historyIndex++
          inputElement.value = this.commandHistory[this.commandHistory.length - this.historyIndex]
        }
      }
      if (event.key === 'ArrowDown') {
        if (this.historyIndex > 1) {
          this.historyIndex--
          inputElement.value = this.commandHistory[this.commandHistory.length - this.historyIndex]
        } else if (this.historyIndex === 1) {
          inputElement.value = ''
        }
      }
    })
  }
  drawCommands () {
    const commandsToPrint = this.commandHistory.slice(Math.max(this.commandHistory.length - 12, 0))
    this.commandListRender.text = commandsToPrint.reduce((string, command) => {
      if (command.length > 25) {
        command = command.substr(0, 25).concat('...')
      }
      return string + '> ' + command + '\n'
    }, '')
  }
  sendCommand (instruction) {
    let words = instruction.split(' ')
    let command = instruction.split(' ')[0]
    words.shift()
    switch (command) {
      case 'up':
        this.room.send(['move', { x: 0, y: -PLAYER_SPEED }])
        break
      case 'down':
        this.room.send(['move', { x: 0, y: PLAYER_SPEED }])
        break
      case 'left':
        this.room.send(['move', { x: -PLAYER_SPEED, y: 0 }])
        break
      case 'right':
        this.room.send(['move', { x: PLAYER_SPEED, y: 0 }])
        break
      case 'stop':
        this.room.send(['move', { x: 0, y: 0 }])
        break
      case 'msg':
        let message = instruction.replace('msg ', '').replace('msg', '')
        this.room.send(['message', { message }])
        break
    }
  }
}
