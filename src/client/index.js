import './style/main.scss'
import Game from './js/game'

let startButton = document.getElementById('button-start')
let graphicsContainer = document.getElementById('game-wrapper')
let gameContainer = document.getElementById('game')
let playerNameInput = document.getElementById('player-name-input')
let inputElement = document.getElementById('command-input')
let mainMenuContainer = document.getElementById('main-menu')

playerNameInput.focus()
startButton.addEventListener('click', () => {
  initGame()
})

playerNameInput.addEventListener('keyup', (event) => {
  if (event.key === 'Enter') {
    initGame()
  }
})

function initGame () {
  mainMenuContainer.style.display = 'none'
  gameContainer.style.display = 'grid'
  let playerName = playerNameInput.value === '' ? 'anon' : playerNameInput.value
  const game = new Game(graphicsContainer, inputElement, playerName)
}
